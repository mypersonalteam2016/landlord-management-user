package com.spalmer.user.model.authority;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

@Document
public class Authority implements GrantedAuthority {
	@Id
	private String id;
	private String authority;

	public Authority() {
	}

	public String getAuthority() {
		return this.authority;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
}

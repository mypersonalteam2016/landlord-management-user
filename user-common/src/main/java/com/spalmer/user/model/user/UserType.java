package com.spalmer.user.model.user;

public enum UserType {
	Landlord,
	Tenant;

	private UserType() {
	}
}


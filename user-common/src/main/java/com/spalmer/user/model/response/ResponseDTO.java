package com.spalmer.user.model.response;

public class ResponseDTO<T> {
	private Boolean success;
	private String message;
	private T response;

	public ResponseDTO() {
	}

	public ResponseDTO(Boolean success, String message, T response) {
		this.success = success;
		this.message = message;
		this.response = response;
	}

	public Boolean getSuccess() {
		return this.success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResponse() {
		return this.response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	public static <T> ResponseDTOBuilder<T> builder() {
		return new ResponseDTOBuilder<>();
	}

	public static class ResponseDTOBuilder<T> {
		private Boolean success;
		private String message;
		private T response;

		public ResponseDTOBuilder() {
		}

		public ResponseDTO.ResponseDTOBuilder success(Boolean success) {
			this.success = success;
			return this;
		}

		public ResponseDTO.ResponseDTOBuilder message(String message) {
			this.message = message;
			return this;
		}

		public ResponseDTO.ResponseDTOBuilder response(T response) {
			this.response = response;
			return this;
		}

		public ResponseDTO build() {
			return new ResponseDTO(this.success, this.message, this.response);
		}
	}
}

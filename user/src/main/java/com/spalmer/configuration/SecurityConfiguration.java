package com.spalmer.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer.AuthorizedUrl;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	public static final String[] ANT_MATCHERS = new String[]{"/user", "/register", "/complete-user-registration", "/username/**"};

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().disable()
		.authorizeRequests().antMatchers(ANT_MATCHERS).permitAll()
				.anyRequest().authenticated()
				.and().csrf().ignoringAntMatchers(ANT_MATCHERS)
				.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
	}
}

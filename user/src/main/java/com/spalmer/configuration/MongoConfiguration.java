package com.spalmer.configuration;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories({"com.spalmer.repository"})
public class MongoConfiguration extends AbstractMongoConfiguration {
	public MongoConfiguration() {
	}

	protected String getDatabaseName() {
		return "housing-tracker-user";
	}

	public Mongo mongo() throws Exception {
		return new MongoClient();
	}
}

package com.spalmer.controller;

import com.spalmer.exception.CannotFindAuthorityException;
import com.spalmer.exception.CannotFindTokenException;
import com.spalmer.exception.UserAlreadyExistsException;
import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.util.Util;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandlerController {
	public ExceptionHandlerController() {
	}

	@ExceptionHandler({UserAlreadyExistsException.class})
	@ResponseBody
	public ResponseDTO userAlreadyExists(UserAlreadyExistsException e) {
		return Util.getErrorResponseDTO("Email already exists");
	}

	@ExceptionHandler({CannotFindAuthorityException.class})
	@ResponseBody
	public ResponseDTO cannotFindAuthority(CannotFindAuthorityException e) {
		return Util.getErrorResponseDTO("An internal error has occurred");
	}

	@ExceptionHandler({CannotFindTokenException.class})
	@ResponseBody
	public ResponseDTO cannotFindToken(CannotFindTokenException e) {
		return Util.getErrorResponseDTO("Registration has failed");
	}

	@ExceptionHandler({Exception.class})
	@ResponseBody
	public ResponseDTO generalException(CannotFindTokenException e) {
		return Util.getErrorResponseDTO("An unexpected error has occurred");
	}
}

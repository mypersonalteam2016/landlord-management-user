package com.spalmer.controller;

import com.spalmer.facade.UserFacade;
import com.spalmer.model.signup.SignUpDTO;
import com.spalmer.model.token.TokenDTO;
import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.user.Tenant;
import com.spalmer.user.model.user.User;
import com.spalmer.util.Util;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	private UserFacade userFacade;

	@Autowired
	public UserController(UserFacade userFacade) {
		this.userFacade = userFacade;
	}

	@RequestMapping("/")
	public Principal user(Principal user) {
		return user;
	}

	@RequestMapping("/{userId}")
	public ResponseDTO<User> getUserByUserId(@PathVariable String userId) {
		User user = userFacade.getUserForUserId(userId);

		return Util.getSucessDTO(user);
	}

	@RequestMapping("/property/{propertyId}")
	public ResponseDTO<List<User>> getUserForProperties(@PathVariable String propertyId) {
		List<User> tenants = userFacade.getUsersForPropertyId(propertyId);
		return Util.getSucessDTO(tenants);
	}

	@RequestMapping("/username/{username}")
	public ResponseDTO<User> getUserByUsername(@PathVariable String username) throws UnsupportedEncodingException {
		User user = userFacade.getUserForUsername(username);
		return Util.getSucessDTO(user);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDTO signup(@RequestBody SignUpDTO dto) {
		userFacade.signup(dto);
		return Util.getSucessDTO();
	}

	@RequestMapping(value = "/complete-user-registration",method = RequestMethod.POST,consumes = "application/json")
	public @ResponseBody ResponseDTO completeRegistration(@RequestBody TokenDTO dto) {
		userFacade.completeRegistration(dto);
		return Util.getSucessDTO();
	}

	@RequestMapping(value = "/delete-user-property/{userId}", method = RequestMethod.POST)
	public @ResponseBody ResponseDTO deleteUserProperty(@PathVariable String userId) {
		userFacade.deleteUserPropertyByUserId(userId);
		return Util.getSucessDTO();
	}

}

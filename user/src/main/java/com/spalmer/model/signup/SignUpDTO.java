package com.spalmer.model.signup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.spalmer.user.model.user.UserType;

@JsonIgnoreProperties(
		ignoreUnknown = true
)
public class SignUpDTO {
	private String name;
	private String chosenPlace;
	private String password;
	private String confirmPassword;
	private String email;
	private UserType userType;
	private String landlordId;
	private String property;

	public SignUpDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConfirmPassword() {
		return this.confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getChosenPlace() {
		return this.chosenPlace;
	}

	public void setChosenPlace(String chosenPlace) {
		this.chosenPlace = chosenPlace;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserType getUserType() {
		return this.userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getLandlordId() {
		return this.landlordId;
	}

	public void setLandlordId(String landlordId) {
		this.landlordId = landlordId;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}
}

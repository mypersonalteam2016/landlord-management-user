package com.spalmer.model.token;

import com.spalmer.user.model.base.BaseDTO;

public class TokenDTO extends BaseDTO {
	private String userId;
	private String token;
	private String emailAddress;

	public TokenDTO(String userId, String token, String emailAddress) {
		this.userId = userId;
		this.token = token;
		this.emailAddress = emailAddress;
	}

	public TokenDTO() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public static class TokenDTOBuilder {
		private String userId;
		private String token;
		private String emailAddress;

		public TokenDTOBuilder() {
		}

		public TokenDTO.TokenDTOBuilder userId(String userId) {
			this.userId = userId;
			return this;
		}

		public TokenDTO.TokenDTOBuilder token(String token) {
			this.token = token;
			return this;
		}

		public TokenDTO.TokenDTOBuilder emailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
			return this;
		}

		public TokenDTO build() {
			return new TokenDTO(this.userId, this.token, this.emailAddress);
		}
	}
}

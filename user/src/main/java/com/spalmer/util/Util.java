package com.spalmer.util;

import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.response.ResponseDTO.ResponseDTOBuilder;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Util {
	public Util() {
	}

	public static ResponseDTO getSucessDTO() {
		return (new ResponseDTOBuilder()).success(Boolean.TRUE).build();
	}

	public static <T> ResponseDTO<T> getSucessDTO(T baseDTO) {
		return new ResponseDTOBuilder<T>().success(Boolean.TRUE)
				.response(baseDTO).build();
	}

	public static ResponseDTO getErrorResponseDTO(String message) {
		return (new ResponseDTOBuilder()).success(Boolean.FALSE).message(message).build();
	}

	public static <T, R> Optional<R> ifNotNullThen(T obj, Function<T,R> function) {
		try {
			Assert.notNull(obj);

			return Optional.of(function.apply(obj));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}
}

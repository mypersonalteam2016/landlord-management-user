package com.spalmer.facade;

import com.spalmer.model.signup.SignUpDTO;
import com.spalmer.model.token.Token;
import com.spalmer.model.token.TokenDTO;
import com.spalmer.model.token.TokenDTO.TokenDTOBuilder;
import com.spalmer.services.UserService;
import com.spalmer.signup.OnSignupEvent;
import com.spalmer.user.model.user.Tenant;
import com.spalmer.user.model.user.User;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class UserFacadeImpl implements UserFacade {
	private UserService userService;
	private ApplicationEventPublisher eventPublisher;

	public UserFacadeImpl(UserService userService, ApplicationEventPublisher eventPublisher) {
		this.userService = userService;
		this.eventPublisher = eventPublisher;
	}

	public TokenDTO signup(SignUpDTO dto) {
		User user = userService.createUser(dto);
		Token token = userService.createToken(user);

		OnSignupEvent onSignupEvent = new OnSignupEvent(this, token, user);
		eventPublisher.publishEvent(onSignupEvent);

		return (new TokenDTOBuilder()).token(token.getToken())
				.userId(user.getId())
				.emailAddress(user.getUsername())
				.build();
	}

	public void completeRegistration(TokenDTO dto) {
		userService.checkTokenAndUpdateUser(dto);
	}

	public void checkTokenIsValidForUser(User user, Token token) {
		userService.checkTokenIsValidForUser(user, token);
	}

	public User getUserForUsername(String userName) throws UnsupportedEncodingException {
		return userService.getUserForUsername(URLDecoder.decode(userName, "UTF-8"));
	}

	@Override
	public User getUserForUserId(String userId) {
		return userService.getUserById(userId);
	}

	@Override
	public List<User> getUsersForPropertyId(String propertyId) {
		return userService.getByProperties(propertyId);
	}

	@Override
	public void deleteUserPropertyByUserId(String userId) {
		userService.deleteUserPropertyByUserId(userId);
	}
}

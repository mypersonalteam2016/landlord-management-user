package com.spalmer.facade;

import com.spalmer.model.signup.SignUpDTO;
import com.spalmer.model.token.Token;
import com.spalmer.model.token.TokenDTO;
import com.spalmer.user.model.user.Tenant;
import com.spalmer.user.model.user.User;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface UserFacade {
	TokenDTO signup(SignUpDTO var1);

	void completeRegistration(TokenDTO var1);

	void checkTokenIsValidForUser(User var1, Token var2);

	User getUserForUsername(String var1) throws UnsupportedEncodingException;

	User getUserForUserId(String userId);

	List<User> getUsersForPropertyId(String propertyId);

	void deleteUserPropertyByUserId(String userId);
}

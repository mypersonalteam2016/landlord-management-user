package com.spalmer.signup;

import com.spalmer.facade.UserFacade;
import com.spalmer.model.dto.NotificationDTO;
import com.spalmer.model.enums.NotificationName;
import com.spalmer.model.token.Token;

import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.user.User;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class OnSignupListener implements ApplicationListener<OnSignupEvent> {

	private RestTemplate restTemplate;
	private UserFacade userFacade;

	@Value("${notifications.baseUrl}")
	private String baseUrl;

	@Autowired
	public OnSignupListener(RestTemplate restTemplate, UserFacade userFacade) {
		this.restTemplate = restTemplate;
		this.userFacade = userFacade;
	}

	public void onApplicationEvent(OnSignupEvent onSignupEvent) {
		Token token = onSignupEvent.getToken();
		User user = onSignupEvent.getUser();
		userFacade.checkTokenIsValidForUser(user, token);
		String url = String.format("%s/%s", token.getToken(), user.getId());
		NotificationDTO dto = getDTO(user, url);
	}

	private NotificationDTO getDTO(User user, String url) {
		return new NotificationDTO.NotificationDTOBuilder()
				.name(NotificationName.SendSignupEmail)
				.email(user.getUsername())
				.content(Collections.singletonMap("url", url))
				.createNotificationDTO();
	}
}

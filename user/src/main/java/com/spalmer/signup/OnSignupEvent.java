package com.spalmer.signup;

import com.spalmer.model.token.Token;
import com.spalmer.user.model.user.User;
import javax.validation.constraints.NotNull;
import org.springframework.context.ApplicationEvent;

public class OnSignupEvent extends ApplicationEvent {
	private Token token;
	private User user;

	public OnSignupEvent(Object source, @NotNull Token token, @NotNull User user) {
		super(source);
		this.token = token;
		this.user = user;
	}

	public Token getToken() {
		return this.token;
	}

	public User getUser() {
		return this.user;
	}
}

package com.spalmer.services;

import com.spalmer.model.signup.SignUpDTO;
import com.spalmer.model.token.Token;
import com.spalmer.model.token.TokenDTO;
import com.spalmer.user.model.user.Tenant;
import com.spalmer.user.model.user.User;

import java.util.List;

public interface UserService {
	User createUser(SignUpDTO var1);

	void saveUser(User var1);

	User getUserForUsername(String var1);

	Token createToken(User var1);

	void checkTokenAndUpdateUser(TokenDTO var1);

	Token getToken(TokenDTO var1, User var2);

	void checkTokenIsValidForUser(User var1, Token var2);

	User getUserById(String id);

	List<User> getByProperties(String propertyId);

	void deleteUserPropertyByUserId(String userId);
}

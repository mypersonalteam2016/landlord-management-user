package com.spalmer.services;

import com.spalmer.exception.CannotFindUserException;
import com.spalmer.exception.ExceptionSupplier;
import com.spalmer.exception.UserAlreadyExistsException;
import com.spalmer.model.UserProperty;
import com.spalmer.model.signup.SignUpDTO;
import com.spalmer.model.token.Token;
import com.spalmer.model.token.TokenDTO;
import com.spalmer.repository.AuthorityRepository;
import com.spalmer.repository.TokenRepository;
import com.spalmer.repository.UserPropertyRepository;
import com.spalmer.repository.UserRepository;
import com.spalmer.user.model.authority.Authority;
import com.spalmer.user.model.user.Landlord;
import com.spalmer.user.model.user.Tenant;
import com.spalmer.user.model.user.User;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.spalmer.user.model.user.UserType;
import com.spalmer.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class UserServiceImpl implements UserService {
	public static final Consumer<User> USER_ALREADY_EXISTS = u -> {
		throw new UserAlreadyExistsException(u.getUsername());
	};
	UserRepository userRepository;
	AuthorityRepository authorityRepository;
	TokenRepository tokenRepository;
	UserPropertyRepository userPropertyRepository;
	PasswordEncoder passwordEncoder;
	Map<UserType, BiFunction<Authority, SignUpDTO, User>> createMap = new HashMap<>(2);

	@Autowired
	public UserServiceImpl(UserRepository userRepository,
	                       AuthorityRepository authorityRepository,
	                       TokenRepository tokenRepository,
	                       PasswordEncoder passwordEncoder,
	                       UserPropertyRepository userPropertyRepository) {
		this.userRepository = userRepository;
		this.authorityRepository = authorityRepository;
		this.tokenRepository = tokenRepository;
		this.passwordEncoder = passwordEncoder;
		this.userPropertyRepository = userPropertyRepository;
		createMap.put(UserType.Landlord, this::createLandlord);
		createMap.put(UserType.Tenant, this::createTenant);
	}

	public User createUser(SignUpDTO dto) {
		Authority authority = authorityRepository.findByAuthority(dto.getUserType().name())
				.orElseThrow(ExceptionSupplier.newCannotFindAuthorityException(dto.getUserType().name()));

		userRepository.findByUsername(dto.getEmail()).ifPresent(USER_ALREADY_EXISTS);

		return createMap.get(dto.getUserType()).apply(authority, dto);
	}

	public void saveUser(User user) {
		this.userRepository.save(user);
	}

	public User getUserById(String id) {
		return userRepository.findOne(id);
	}

	@Override
	public List<User> getByProperties(String propertyId) {
		List<String> userIds = userPropertyRepository.findByPropertyId(propertyId).stream()
				.map(UserProperty::getUserId)
				.collect(Collectors.toList());

		Iterable<User> userIterator = userRepository.findAll(userIds);

		return StreamSupport.stream(userIterator.spliterator(), false)
				.collect(Collectors.toList());
	}

	public User getUserForUsername(String username) {
		return userRepository.findByUsername(username).orElseThrow(ExceptionSupplier.newUserNotFoundException(username));
	}

	public Token createToken(User user) {
		Token token = new Token();
		token.setUser(user);
		token.setToken(UUID.randomUUID().toString());
		tokenRepository.save(token);
		return token;
	}

	public void checkTokenAndUpdateUser(TokenDTO dto) {
		User user = this.getUserById(dto);
		Token token = this.getToken(dto, user);
		user.setEnabled(Boolean.TRUE);
		userRepository.save(user);
		tokenRepository.delete(token);
	}

	public Token getToken(TokenDTO dto, User user) {
		Optional<Token> tokenOptional = tokenRepository.findByTokenAndUser(dto.getToken(), user);
		return tokenOptional
				.orElseThrow(ExceptionSupplier.newCannotFindTokenException(dto));
	}

	public void checkTokenIsValidForUser(User user, Token token) {
		this.userRepository.findByUsername(token.getUser().getUsername())
				.orElseThrow(ExceptionSupplier.newUserAndTokenMismatchException(user));
	}

	private User getUserById(TokenDTO dto) {
		User user = userRepository.findOne(dto.getUserId());

		try {
			Assert.notNull(user);
			return user;
		} catch (IllegalArgumentException var4) {
			throw new CannotFindUserException(String.format("Cannot find user %s", user.getId()));
		}
	}

	private User createLandlord(Authority authority, SignUpDTO dto) {
		Landlord newUser = new Landlord();
		setCommonUserFields(authority, dto, newUser);
		userRepository.save(newUser);
		return newUser;
	}

	private void setCommonUserFields(Authority authority, SignUpDTO dto, User newUser) {
		newUser.setUsername(dto.getEmail());

		Util.ifNotNullThen(dto.getPassword(), passwordEncoder::encode)
				.ifPresent(newUser::setPassword);

		newUser.setName(dto.getName());
		newUser.setAccountNonExpired(true);
		newUser.setAccountNonLocked(true);
		newUser.setCredentialsNonExpired(true);
		newUser.setEnabled(false);
		newUser.setAuthorities(Collections.singletonList(authority));
	}

	private User createTenant(Authority authority, SignUpDTO dto) {
		Tenant newUser = new Tenant();
		setCommonUserFields(authority, dto, newUser);
		userRepository.save(newUser);

		UserProperty userProperty = createUserProperty(dto, newUser.getId());
		userPropertyRepository.save(userProperty);

		return newUser;
	}

	private UserProperty createUserProperty(SignUpDTO dto, String userId) {
		UserProperty userProperty = new UserProperty();

		userProperty.setUserId(userId);
		userProperty.setPropertyId(dto.getProperty());
		userProperty.setLandlordId(dto.getLandlordId());

		return userProperty;
	}

	@Override
	public void deleteUserPropertyByUserId(String userId) {
		UserProperty userProperty = userPropertyRepository.findByUserId(userId);
		userPropertyRepository.delete(userProperty);
	}
}

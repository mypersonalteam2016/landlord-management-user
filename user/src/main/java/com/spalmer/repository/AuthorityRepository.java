package com.spalmer.repository;

import com.spalmer.user.model.authority.Authority;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends MongoRepository<Authority, String> {
	Optional<Authority> findByAuthority(String var1);
}

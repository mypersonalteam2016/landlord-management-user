package com.spalmer.repository;

import com.spalmer.model.UserProperty;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserPropertyRepository extends MongoRepository<UserProperty, String> {

	List<UserProperty> findByPropertyId(String propertyId);

	UserProperty findByUserId(String userId);
}

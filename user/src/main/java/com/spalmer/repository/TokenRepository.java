package com.spalmer.repository;

import com.spalmer.model.token.Token;
import com.spalmer.user.model.user.User;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends MongoRepository<Token, String> {
	Optional<Token> findByToken(String var1);

	Optional<Token> findByTokenAndUser(String var1, User var2);
}
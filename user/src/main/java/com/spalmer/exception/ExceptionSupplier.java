package com.spalmer.exception;

import com.spalmer.exception.CannotFindAuthorityException;
import com.spalmer.exception.CannotFindTokenException;
import com.spalmer.exception.CannotFindUserException;
import com.spalmer.exception.UserAndTokenMismatchException;
import com.spalmer.model.token.TokenDTO;
import com.spalmer.user.model.user.User;
import java.util.function.Supplier;

public class ExceptionSupplier {
	public ExceptionSupplier() {
	}

	public static Supplier<CannotFindAuthorityException> newCannotFindAuthorityException(String authority) {
		String message = String.format("Cannot find authority with name %s", new Object[]{authority});
		return () -> {
			return new CannotFindAuthorityException(message);
		};
	}

	public static Supplier<CannotFindTokenException> newCannotFindTokenException(TokenDTO dto) {
		String message = String.format("Cannot find token %s for user %s", new Object[]{dto.getToken(), dto.getUserId()});
		return () -> {
			return new CannotFindTokenException(message);
		};
	}

	public static Supplier<UserAndTokenMismatchException> newUserAndTokenMismatchException(User user) {
		String message = String.format("An invalid token has been supplied for user %s", new Object[]{user.getId()});
		return () -> {
			return new UserAndTokenMismatchException(message);
		};
	}

	public static Supplier<CannotFindUserException> newUserNotFoundException(String username) {
		String message = String.format("Cannot find user for username %s", new Object[]{username});
		return () -> {
			return new CannotFindUserException(message);
		};
	}
}

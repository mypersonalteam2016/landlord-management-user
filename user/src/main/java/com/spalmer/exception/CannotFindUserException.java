package com.spalmer.exception;

public class CannotFindUserException extends RuntimeException {
	public CannotFindUserException(String message) {
		super(message);
	}
}

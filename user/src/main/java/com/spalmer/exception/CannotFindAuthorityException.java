package com.spalmer.exception;

public class CannotFindAuthorityException extends RuntimeException {
	public CannotFindAuthorityException(String message) {
		super(message);
	}
}

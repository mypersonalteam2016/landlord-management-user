package com.spalmer.exception;

public class CannotFindTokenException extends RuntimeException {
	public CannotFindTokenException(String message) {
		super(message);
	}
}

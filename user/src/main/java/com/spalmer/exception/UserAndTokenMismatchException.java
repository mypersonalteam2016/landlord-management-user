package com.spalmer.exception;


public class UserAndTokenMismatchException extends RuntimeException {
	public UserAndTokenMismatchException(String message) {
		super(message);
	}
}
